#!/usr/bin/python

import argparse
import signal
import json
import time
import logging
import paho.mqtt.client as mqtt
from opcua import Client

class TagServer:
    def __init__(self, config):
        self.client = self.setupClient()
        self.tags = self.loadConfig(config)
        self.mqttClient = self.setupMqtt()
        self.interrupted = False
        signal.signal(signal.SIGINT, self.signalHandler)

    def loadConfig(self, config):
        with open(config) as f:
            data = json.load(f)
            print data
        return data

    def mqttConnect(self, client, userdata, flags, rc):
	print "Setting up connections"

    def mqttMessage(self, client, userdata, msg):
	print "Setting up message handler"

    def run(self):
        try:
            self.client.connect()
            self.mqttClient.connect("localhost")
            self.mqttClient.loop_start()

            while True:
                for index, item in enumerate(self.tags['tags']):
                    item[u"cv"] = self.client.get_node(str(item[u"address"])).get_value()
                    payload = "met="+str(item[u"name"])+"~data="+str(item[u"cv"])
                    print "payload: "+payload
		    self.mqttClient.publish("IQP_Gescan/CPL410",payload)

                time.sleep(1)

                if self.interrupted:
                    break
        finally:
            self.mqttClient.loop_stop(force=False)
            self.mqttClient.disconnect()
            self.client.disconnect()


    def setupClient(self):
        return Client("opc.tcp://192.168.180.2:4840")

    def setupMqtt(self):
        mqttClient = mqtt.Client()
        mqttClient.on_connect = self.mqttConnect
        mqttClient.on_message = self.mqttMessage
        return  mqttClient

    def signalHandler(self, signal, frame):
        self.interrupted = True

def parse_args():
    parser = argparse.ArgumentParser(
        description='Example Tag Server Backend Script for Operations Hub')
    parser.add_argument('--config', type=str, required=False,
                        default='./tagconfig.json',
                        help='path to the configuration file')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    server = TagServer(args.config)
    server.run()
